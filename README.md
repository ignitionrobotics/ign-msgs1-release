# ign-msgs1-release

The ign-msgs1-release repository has moved to: https://github.com/ignition-release/ign-msgs1-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-msgs1-release
